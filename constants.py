# DB TABLES
USER_TABLE = 'quiz_user'
SCORE_TABLE = 'quiz_score'
QUIZ_TABLE = 'quiz'
QUIZ_QA_TABLE = 'quiz_question_ans'
QUIZ_USER_RESPONSE_TABLE = 'quiz_user_response'


# Other

QUIZ_KW = 'quiz'
QUIZ_QA_KW = 'QuizQA'
Q_RESPONSE_KW = 'q_response'
USER_ID_KW = 'user_id'
USER_KW = 'user'
QUIZ_ID_KW = 'quiz_id'