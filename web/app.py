from pprint import pprint
from wsgiref.simple_server import make_server

from .render import (
    home, render_login, not_found,
    handle_login
)
from .auth import BasicAuth
from db.sqlitedb import SQLiteDatabse
from repos.user import UserRepository

DB_PATH = 'db.sqlite'
db = SQLiteDatabse(DB_PATH)
user_repo = UserRepository(db=db)


def _app(environ, start_response):
    # response_body = b"Hello, World!"
    path = environ.get("PATH_INFO")
    method = environ['REQUEST_METHOD']

    # pprint(environ)
    if path.endswith("/"):
        path = path[:-1]

    if path == "":
        data = home(environ)
    elif path == "/login":
        if method == 'GET':
            data = render_login(environ)
        else:
            data = handle_login(environ, start_response, user_repo)
    else:
        data = not_found(environ)
    data = data.encode('utf-8')
    status = "200 OK"
    start_response(status, headers=[
        ('Content-Type', "text/html"),
        ("Content-Length", str(len(data)))
    ])
    return iter([data])


# def app(environ, start_response):
#     environ['user'] = ''
#     return _app(environ, start_response)


app = BasicAuth(_app)
PORT = 8001

with make_server('', PORT, app) as httpd:
    print('Visit localhost:{}'.format(PORT))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()