import datetime


def set_cookie_header(name, value, days=365):
    dt = datetime.datetime.now() + datetime.timedelta(days=days)
    fdt = dt.strftime('%a, %d %b %Y %H:%M:%S GMT')
    secs = days * 86400
    return ('Set-Cookie', '{}={}; Expires={}; Max-Age={}; Path=/'.format(name, value, fdt, secs))


def handler(env, start_response):
    content_type = 'text/html'
    headers = [('Content-Type', content_type), set_cookie_header('name', 'value')]
    start_response('200 OK', headers)


class BasicAuth:

    def __init__(self, app, realm="quiz_app", ):
        self._app = app
        self._realm = realm


    def __call__(self, environ, start_response):
        environ['quiz.user.authorize'] = self.authorize
        environ['app.user'] = 'XXX'
        return self._app(environ, start_response)

    def authorize(self, environ, status_response, encoded, logout=False):
        print(encoded)
        if logout:
            environ['HTTP_AUTHORIZATION'] = ""
            return None

        environ['WWW-Authenticate'] = encoded
        environ['app.user'] = encoded
        # headers = [('Content-Type', 'text/html'),
        #      ('WWW-Authenticate', 'Basic realm="Login"')]
        # set_cookie_header("quiz.user", encoded)
        # status_response('200 OK', headers)

        return True
