from urllib.parse import parse_qs


def render_template(template_name='web/templates/index.html', context={}):
    html_str = ""
    with open(template_name, 'r') as f:
        html_str = f.read()

    return html_str.format(**context)


def home(environ):
    path = environ.get("PATH_INFO")
    return render_template(
        template_name="web/templates/index.html",
        context={"path": path}
    )


def render_login(environ):
    path = environ.get("PATH_INFO")
    return render_template(
        template_name="web/templates/login.html",
        context={"path": path}
    )


def parse_data(environ):
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except ValueError:
        request_body_size = 0
    request_body = environ['wsgi.input'].read(request_body_size)
    return parse_qs(request_body.decode('utf-8'))


def handle_login(environ, status_response, user_repo):
    data = parse_data(environ)
    email = data.get('email', None)
    password = data.get('password', None)
    # print(email, password)
    if email and password:
        user = user_repo.login(email[0], password[0])
        if user:
            from base64 import b64encode
            encoded = b64encode('Basic realm="Login" {} {}'.format(user.email, user.role).encode('utf-8')).decode('utf-8')
            print('handle_login ', user)
            environ['quiz.user.authorize'](environ,status_response, encoded)
        # environ['user'] = 'sdsdfsdfsd'  # user.email.encode('utf-8')
        return render_template(
            template_name="web/templates/index.html",
            context={"path": ''}
        )
    return render_template(
        template_name="web/templates/404.html",
        context={"path": ''}
    )


def not_found(environ):
    path = environ.get("PATH_INFO")
    return render_template(
        template_name="web/templates/404.html",
        context={"path": path}
    )
