from dataclasses import dataclass

'''
Tables:
    User : id:int, name:str, email: str, password: str, role:str teacher|student
    Score : userid: int, quizID: int, score: int
    Quiz : Id: int, created_by: Userid, Quiz_name: str, description: str, question_count: int, created_at: datetime.datetime
    QuizQA : QuizQAID: int, QuizID: int, question: str, op1: str, op2: str, op3: str, op4: str, ans: str
    QuizUserResponse: responseid: int, userid: int, quizID: int, quizQAID: int, response: str
'''


@dataclass
class User:
    email: str
    name: str
    Id: int = None
    password: str = ''
    role: str = 'student'

    def db_sql_params(self):
        return self.email, self.name, self.password, self.role

    @staticmethod
    def create_user(user_data):
        Id, email, name, role = user_data
        return User(Id=Id, email=email, name=name, role=role)


if __name__ == '__main__':
    u = User('email@mail.com', 'hasib')
    u.role = ""
    u.password = '1212'
    print(u)
