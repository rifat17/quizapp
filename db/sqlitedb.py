import sqlite3

from .base import AbstractDatabase, SafeCursor
from constants import (
    USER_TABLE, SCORE_TABLE, QUIZ_TABLE, QUIZ_QA_TABLE, QUIZ_USER_RESPONSE_TABLE
)

'''
Tables:
    User: Id auto increment, email: str,name:str, id:int, password: str, role:str teacher|student
    Score: Id auto increment, userid: int, quizID: int, score: int
    Quiz : Id auto increment, created_by: Userid, Quiz_name: str, description: str, question_count: int, created_at: str datetime.datetime
    QuizQA : Id auto increment, quiz_Id: int, question: str, opt1: str, opt2: str, opt3: str, opt4: str, correct_ans: str
    QuizUserResponse: Id auto increment, userid: int, quizID: int, quizQAID: int, response: str
'''


def user_table_creation_sql():
    sql = f'''
    CREATE TABLE IF NOT EXISTS {USER_TABLE}(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    email VARCHAR(50) NOT NULL UNIQUE,
    name VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    role VARCHAR(20) NOT NULL);
    '''
    return sql


'''
INSERT INTO quiz_user (id, name, email, password, role) 
VALUES(1, "hasib", "hasib@mail.com", "123456", "student")

SELECT id, name, email, role from quiz_user where email="hasib@mail.com" AND password="123456";
'''


def quiz_user_response_table_creation_sql():
    sql = f'''
    CREATE TABLE IF NOT EXISTS {QUIZ_USER_RESPONSE_TABLE}(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_Id INT NOT NULL REFERENCES {USER_TABLE}(Id) ON UPDATE CASCADE ON DELETE CASCADE ,
    quiz_Id INT NOT NULL REFERENCES {QUIZ_TABLE}(Id) ON UPDATE CASCADE ON DELETE CASCADE , 
    quiz_Qa_Id INT NOT NULL REFERENCES {QUIZ_QA_TABLE}(Id) ON UPDATE CASCADE ON DELETE CASCADE , 
    response VARCHAR(50) NOT NULL);
    '''
    return sql


def quiz_qa_table_creation_sql():
    sql = f'''
    CREATE TABLE IF NOT EXISTS {QUIZ_QA_TABLE}(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    quiz_Id INT NOT NULL REFERENCES {QUIZ_TABLE}(Id) ON UPDATE CASCADE ON DELETE CASCADE ,
    question VARCHAR(50) NOT NULL,
    opt1 VARCHAR(50) NOT NULL,
    opt2 VARCHAR(50) NOT NULL,
    opt3 VARCHAR(50) NOT NULL,
    opt4 VARCHAR(50) NOT NULL,
    correct_ans VARCHAR(50) NOT NULL);
    '''
    return sql


def quiz_table_creation_sql():
    sql = f'''
    CREATE TABLE IF NOT EXISTS {QUIZ_TABLE}(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    created_by INT NOT NULL REFERENCES {USER_TABLE}(Id) ON UPDATE CASCADE ON DELETE CASCADE ,
    quiz_name VARCHAR(50) NOT NULL,
    description VARCHAR(50),
    question_count INT NOT NULL,
    created_at VARCHAR(50) NOT NULL
    );
    '''
    return sql


# FOREIGN KEY(created_by) REFERENCES {USER_TABLE} (Id)

def quiz_score_table_creation_sql():
    sql = f'''
    CREATE TABLE IF NOT EXISTS {SCORE_TABLE}(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_Id INT NOT NULL REFERENCES {USER_TABLE}(Id) ON UPDATE CASCADE ON DELETE CASCADE ,
    quizId INT NOT NULL REFERENCES {QUIZ_TABLE}(Id) ON UPDATE CASCADE ON DELETE CASCADE , 
    score INT NOT NULL) ;
    '''
    return sql


class SQLiteDatabse(AbstractDatabase):

    def __init__(self, db_path='db.sqlite'):
        self._conn = sqlite3.connect(db_path)
        self._create_user_table()
        self._create_quiz_table()
        self._create_quiz_qa_table()
        self._create_quiz_score_table()
        self._create_quiz_user_response_table()

    def _create_user_table(self):
        with SafeCursor(self._conn, commit=True) as cur:
            cur.execute(user_table_creation_sql())

    def _create_quiz_table(self):
        with SafeCursor(self._conn, commit=True) as cur:
            cur.execute(quiz_table_creation_sql())

    def _create_quiz_qa_table(self):
        with SafeCursor(self._conn, commit=True) as cur:
            cur.execute(quiz_qa_table_creation_sql())

    def _create_quiz_score_table(self):
        with SafeCursor(self._conn, commit=True) as cur:
            cur.execute(quiz_score_table_creation_sql())

    def _create_quiz_user_response_table(self):
        with SafeCursor(self._conn, commit=True) as cur:
            cur.execute(quiz_user_response_table_creation_sql())

    def select(self, value):
        with SafeCursor(self._conn) as cur:
            sql, params = value
            # print(sql)
            # print(params)
            if params:
                cur.execute(sql, params)
            else:
                cur.execute(sql)
            fetched_value = cur.fetchall()
            return fetched_value

    def insert(self, value):
        with SafeCursor(connection=self._conn, commit=True) as cur:
            sql, params = value
            try:
                cur.execute(sql, params)
            except sqlite3.IntegrityError as err:
                print(err)
