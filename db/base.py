import abc


class AbstractDatabase(abc.ABC):

    @abc.abstractmethod
    def select(self, value):
        raise NotImplementedError

    @abc.abstractmethod
    def insert(self, value):
        raise NotImplementedError


class AbstractRepository(abc.ABC):

    @abc.abstractmethod
    def get(self, **kwargs):
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, **kwargs):
        raise NotImplementedError


class SafeCursor:
    def __init__(self, connection, commit=False):
        self.con = connection
        self.require_commit = commit

    def __enter__(self):
        self.cursor = self.con.cursor()
        return self.cursor

    def __exit__(self, typ, value, traceback):
        if self.require_commit:
            self.con.commit()
        self.cursor.close()
