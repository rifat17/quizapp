from db.base import AbstractRepository, AbstractDatabase
from models.quiz import Score

from constants import (
    USER_ID_KW, QUIZ_ID_KW, SCORE_TABLE
)


class ScoreRepository(AbstractRepository):
    def __init__(self, db: AbstractDatabase):
        self._db = db

    def get(self, **kwargs):
        '''
        :param kwargs:
         user_id : int
         quiz_id : int
        :return:
        an instance of Score
        '''
        user_id = kwargs.get(USER_ID_KW, None)
        quiz_id = kwargs.get(QUIZ_ID_KW, None)
        if not user_id and quiz_id:
            raise KeyError

        # TODO: change table column name quizId => quiz_Id
        sql = f'''
                SELECT * FROM {SCORE_TABLE}
                WHERE user_Id = {user_id} AND quizId = {quiz_id};
                '''
        score = self._db.select((sql, None))[0]
        # print(score)

        return Score.create_score(score_data=score)

    def add(self, **kwargs):
        score: Score = kwargs.get('score', None)
        if not score:
            raise KeyError
        # TODO: change table column name quizId => quiz_Id
        sql = f'''
        INSERT INTO {SCORE_TABLE}
        (user_id, quizId, score) VALUES (?,?,?)
        '''
        # (score.userId, score.quizId, score.score)
        params = score.db_sql_params()
        self._db.insert((sql, params))
