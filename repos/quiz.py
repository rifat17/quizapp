from db.base import AbstractRepository, AbstractDatabase
from models.quiz import Quiz
from constants import (
    QUIZ_TABLE, QUIZ_KW
)


class QuizRepository(AbstractRepository):
    def __init__(self, db: AbstractDatabase):
        self._db = db

    def get(self, **kwargs):
        '''GET ALL QUIZ'''
        all = kwargs.get('all', None)
        sql = ''
        if all:
            sql = f'''
        SELECT * FROM {QUIZ_TABLE};
        '''
        quizs = self._db.select((sql, None))
        # print(quizs)
        quizs_list = []
        for quiz in quizs:
            # print(quiz)
            quizs_list.append(Quiz.create_quiz(quiz_data=quiz))

        return quizs_list

    def add(self, **kwargs):
        quiz = kwargs[QUIZ_KW]
        sql = f'''
        INSERT INTO {QUIZ_TABLE}
        (created_by, quiz_name, description, question_count, created_at) VALUES (?,?,?,?,?);
        '''
        # (quiz.created_by, quiz.name, quiz.description, quiz.question_count, str(quiz.created_at))
        params = quiz.db_sql_params()
        self._db.insert((sql, params))
