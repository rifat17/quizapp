from db.base import AbstractRepository, AbstractDatabase
from models.user import User

from constants import (
    USER_TABLE, USER_KW
)


class UserRepository(AbstractRepository):
    def __init__(self, db: AbstractDatabase):
        self._db = db

    def get(self, **kwargs):
        pass

    def login(self, email, password):
        sql = f'''
        SELECT Id, email, name, role
        FROM {USER_TABLE}
        where email= ? AND password=?;
        '''
        query = (sql, (email, password))
        # print(query)
        user = self._db.select(query)
        if user:
            return User.create_user(user_data=user[0])
        return None

    def add(self, **kwargs):
        # TODO: Hash password before save
        user = kwargs[USER_KW]
        sql = f'''
        INSERT INTO {USER_TABLE}
        (email, name, password, role) VALUES (?,?,?,?)
        '''
        query = (sql, user.db_sql_params())
        # print(query)
        self._db.insert(query)

    def get_by_id(self, id):
        sql = f'''
                SELECT Id, email, name, role
                FROM {USER_TABLE}
                where Id= ?;
                '''
        query = (sql, (id,))
        # print(query)
        user = self._db.select(query)
        if user:
            return User.create_user(user_data=user[0])
        return None

    def get_by_email(self, email):
        sql = f'''
                SELECT Id, email, name, role
                FROM {USER_TABLE}
                where email= ?;
                '''
        query = (sql, (email,))
        # print(query)
        user = self._db.select(query)
        if user:
            return User.create_user(user_data=user[0])
        return None

    def get_password(self, email):
        sql = f'''
            SELECT password
            FROM {USER_TABLE}
            where email= ?;
            '''
        query = (sql, (email,))
        # print(query)
        password = self._db.select(query)
        if password:
            return password[0]
        return None
