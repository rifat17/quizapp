from db.base import AbstractRepository, AbstractDatabase
from models.quiz import QuizQA

from constants import (
    QUIZ_ID_KW, QUIZ_QA_TABLE, QUIZ_QA_KW
)


class QuizQARepository(AbstractRepository):
    def __init__(self, db: AbstractDatabase):
        self._db = db

    def get(self, **kwargs):
        quiz_id = kwargs.get(QUIZ_ID_KW, None)
        sql = ''
        if quiz_id:
            sql = f'''
            SELECT * FROM {QUIZ_QA_TABLE}
            WHERE quiz_id = {quiz_id};
            '''
        qas = self._db.select((sql, None))
        qa_list = []
        for qa in qas:
            qa_list.append(QuizQA.create_quiz_qa(quiz_qa_data=qa))

        return qa_list

    def add(self, **kwargs):
        '''
        :param kwargs:
        QuizQA : QuizQA
        :return:
        '''
        quizQA: QuizQA = kwargs.get(QUIZ_QA_KW, None)
        # QuizQA : Id auto increment, quiz_Id: int, question: str, opt1: str, opt2: str, opt3: str, opt4: str, ans: str
        sql = f'''
        INSERT INTO {QUIZ_QA_TABLE}
        (quiz_Id, question, opt1, opt2, opt3, opt4, correct_ans ) VALUES (?,?,?,?,?,?,?);
        '''
        query = (sql, quizQA.db_sql_params())

        self._db.insert(query)
