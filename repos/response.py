from db.base import AbstractRepository, AbstractDatabase
from models.quiz import QuizUserResponse

from constants import (
    USER_ID_KW, QUIZ_ID_KW, QUIZ_USER_RESPONSE_TABLE, Q_RESPONSE_KW,
)


class QuizUserResponseRepository(AbstractRepository):
    def __init__(self, db: AbstractDatabase):
        self._db = db

    def get(self, **kwargs):
        '''Get All response from User X for Quiz Y'''
        user_id = kwargs.get(USER_ID_KW, None)
        quiz_id = kwargs.get(QUIZ_ID_KW, None)
        if not user_id and quiz_id:
            raise KeyError
        sql = f'''
        SELECT * FROM {QUIZ_USER_RESPONSE_TABLE}
        WHERE user_Id = {user_id} and quiz_Id = {quiz_id};
        '''
        quiz_responses = self._db.select((sql, None))

        quiz_response_list = []
        for quiz_response in quiz_responses:
            quiz_response_list.append(QuizUserResponse.create_quiz_user_response(user_response=quiz_response))

        return quiz_response_list

    def add(self, **kwargs):
        q_response: QuizUserResponse = kwargs.get(Q_RESPONSE_KW, None)
        sql = ''
        if q_response:
            sql = f'''
            INSERT INTO {QUIZ_USER_RESPONSE_TABLE}
            (user_id, quiz_id, quiz_Qa_Id, response) VALUES (?, ?, ?, ?);
            '''
        query = (sql, q_response.db_sql_params())
        self._db.insert(query)
