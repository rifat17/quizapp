from db.sqlitedb import SQLiteDatabse
from repos.user import UserRepository
from repos.quiz import QuizRepository
from repos.quizqa import QuizQARepository
from repos.response import QuizUserResponseRepository
from repos.score import ScoreRepository
from models.user import User
from models.quiz import Quiz, QuizQA, QuizUserResponse, Score

if __name__ == '__main__':
    db = SQLiteDatabse()
    user_repo = UserRepository(db=db)
    user = User(email='hasib@mail.com', password='12345', name='hasib')
    # print(user)
    # user_repo.add(user=user)
    user = user_repo.login(email='hasib@mail.com', password='12345')

    quiz = Quiz(created_by=user.Id, name='CSE101', description='CT01')
    qrepo = QuizRepository(db=db)
    # qrepo.add(quiz=quiz)
    for q in qrepo.get(all=True):
        print(q)

    q = qrepo.get(all=True)[0]

    qa1 = QuizQA(q.Id, 'Color of sky is?', 'red', 'green', 'blue', 'black', 'blue')
    # print(qa1)

    qa2 = QuizQA(q.Id, 'Color of night is?', 'red', 'green', 'blue', 'black', 'blue')
    # print(qa2)
    qa3 = QuizQA(q.Id, 'Color of moon is?', 'red', 'white', 'blue', 'black', 'white')

    qa_repo = QuizQARepository(db)

    # qa_repo.add(QuizQA=qa1)
    # qa_repo.add(QuizQA=qa2)
    # qa_repo.add(QuizQA=qa3)

    # for qa in qa_repo.get(quiz_id=q.Id):
    #     print(qa)

    q_res1 = QuizUserResponse(userId=user.Id, quizId=q.Id, quizQAId=1, response='blue')
    q_res2 = QuizUserResponse(userId=user.Id, quizId=q.Id, quizQAId=2, response='white')
    q_res3 = QuizUserResponse(userId=user.Id, quizId=q.Id, quizQAId=3, response='blue')
    # print(q_res)
    qr_repo = QuizUserResponseRepository(db=db)
    # qr_repo.add(q_response=q_res1)
    # qr_repo.add(q_response=q_res2)
    # qr_repo.add(q_response=q_res3)

    q_a = qa_repo.get(quiz_id=q.Id)
    q_res = qr_repo.get(user_id=user.Id, quiz_id=q.Id)
    # print(len(q_res))
    # print(len(q_a))
    # print(q_a)
    # print(q_res)
    point = 0
    for expected, actual  in zip(q_a, q_res):
        print("Correct Ans : ", expected.ans,", User Ans : ", actual.response)
        if expected.ans == actual.response:
            point += 1
    score = Score(userId=user.Id, quizId=q.Id, score=point)
    score_repo = ScoreRepository(db=db)
    # score_repo.add(score=score)
    print(score_repo.get(user_id=user.Id, quiz_id=q.Id))
    print(user_repo.get_by_id(score.userId))






